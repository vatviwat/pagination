import React, { Component } from 'react'
import {Container,Table,PageItem,Pagination} from 'react-bootstrap'
export default class Test extends Component {
    constructor(props){
        super(props);
        this.state = {
            info: [
                {
                    id: 1,
                    name: "bob"
                },
                {
                    id: 2,
                    name: "jack"
                },
                {
                    id: 2,
                    name: "jack"
                },
                {
                    id: 2,
                    name: "jack"
                },
                {
                    id: 2,
                    name: "jack"
                },
                {
                    id: 2,
                    name: "jack"
                },
                {
                    id: 2,
                    name: "jack"
                },

            ],
            currentPage: 1,
            perPage: 3
        }
        this.nextPage = this.nextPage.bind(this);
        this.prevPage = this.prevPage.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(event) {
        this.setState({
          currentPage: Number(event.target.id)
        });
    }
    nextPage() {
        let count = Math.ceil(this.state.info.length / this.state.perPage)
        if (this.state.currentPage < count){
          this.setState({
            currentPage: this.state.currentPage + 1
          });
        }   
    }
    prevPage() {
        if (this.state.currentPage>1){
          this.setState({
            currentPage: this.state.currentPage - 1
          });
    }
    }
    render() {
        const indexOfLastTodo = this.state.currentPage * this.state.perPage;
        const indexOfFirstTodo = indexOfLastTodo - this.state.perPage;
        const currentTodos = this.state.info.slice(indexOfFirstTodo, indexOfLastTodo);
        let infoList = currentTodos.map((people, index) => 
            <tbody>
                <td key={index}>{people.id}</td>
                <td>{people.name}</td>
            </tbody>
        );
        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(this.state.info.length / this.state.perPage); i++) {
          pageNumbers.push(i);
        }

        let renderPageNumbers = pageNumbers.map(number =>
            <Pagination.Item
              key={number}
              id={number}
              onClick={this.handleClick}>
              {number}
            </Pagination.Item>
          );
        return (
            <Container>
            <Table style={{textAlign: "center"}}>
                {infoList}
            </Table>
            <Pagination>
              <PageItem onClick={this.prevPage} role="button for change list">Prev</PageItem>
                {renderPageNumbers} 
              <PageItem onClick={this.nextPage} role="button for change list">Next</PageItem>
            </Pagination>
          </Container>
        )
    }
}

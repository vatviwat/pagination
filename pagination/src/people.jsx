import React from 'react'
let people = [];
class People {
  constructor(name,age){
    this.name = name;
    this.age = age;
    this.id = people.length
    people.push(this);
  }  
}

new People('bob','10');
new People('jack', '12');
new People('dede', '3');
new People('joe', '3');
new People('marky', '3');
new People('bob','10');
new People('jack', '12');
new People('dede', '3');
new People('joe', '3');
new People('marky', '3');
new People('bob','10');
new People('jack', '12');
new People('dede', '3');
new People('joe', '3');
new People('marky', '3');
new People('bob','10');
new People('jack', '12');
new People('dede', '3');
new People('joe', '3');
new People('marky', '3');

export default people;

import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Pagination,Table,PageItem, Container} from 'react-bootstrap';
export default class MyPage extends Component {
    constructor(props) {
        super(props);
        let params = (window.location.search != "") ? params = window.location.search.match(/page=\d*/)[0].split('=')[1] : 1      
        params = (params <= 0) ? 1 : params;
        console.log(params);
            
        this.state = {
          people: props.cart,
          currentPage: Number(params),
          todosPerPage: 3
        };
        this.handleClick = this.handleClick.bind(this);
        this.nextPage = this.nextPage.bind(this);
        this.prevPage = this.prevPage.bind(this);
      } 
    
      handleClick(event) {
        this.setState({
          currentPage: Number(event.target.id)
        });
      }
      nextPage() {
        let count = Math.ceil(this.state.people.length / this.state.todosPerPage)
        if (this.state.currentPage < count){
          this.setState({
            currentPage: this.state.currentPage + 1
          });
        }   
      }
    
      prevPage() {
        if (this.state.currentPage>1){
          this.setState({
            currentPage: this.state.currentPage - 1
          });
      }
      }
    
      render() {
        const { people, currentPage, todosPerPage } = this.state;
    
        // Logic for displaying current todos
        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        const currentTodos = people.slice(indexOfFirstTodo, indexOfLastTodo);
        let renderTodos = currentTodos.map((people, index) => 
        <tbody>
            <td key={index}>{people.name}, age={people.age}</td>
            </tbody>
        );
    
        // Logic for displaying page numbers
        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(people.length / todosPerPage); i++) {
          pageNumbers.push(i);
        }
        
        let renderPageNumbers = pageNumbers.map(number =>
            <Pagination.Item
              key={number}
              id={number}
              onClick={this.handleClick}>
              {number}
            </Pagination.Item>
          );
        return (
          <Container>
            <Table style={{textAlign: "center"}}>
                {renderTodos}
            </Table>
            <Pagination>
              <PageItem onClick={this.prevPage} role="button for change list">Prev</PageItem>
                {renderPageNumbers}
              <PageItem onClick={this.nextPage} role="button for change list">Next</PageItem>
            </Pagination>
          </Container>
        );
      }
}
